import React, { Component } from 'react';
// import axios from 'axios'
import './App.css';

/*
* formValid returnerer enten true eller false, ut ifra hva slags feil vi har lagret i formErrors. La til ekstra if for
* å forhindre at tomme skjema kan sendes inn.
* */

const formValid = ({ formErrors, name, email, phone, areacode }) => {
  let validation = true;
  Object.values(formErrors).forEach(value => {
    if (value.length > 0) validation = false;
  });
  if (name.length === 0 || email.length === 0 || phone.length === 0 || areacode.length === 0) validation = false;
  return validation;
};

class App extends Component {
  constructor (props) {
    super(props);
    this.state = {
      applicant: 'Jesper',
      areacode: 0,
      comment: '',
      disabled: true,
      email: '',
      formErrors: {
        name: '',
        email: '',
        phone: '',
        areacode: 0,
        comment: '',
      },
      name: '',
      phone: ''
    }
  };

  handleSubmit = e => {
    // Fikk No-Cors med denne løsningen. Gikk for den gode gamle ved å sende formet direkte.
   /* e.preventDefault() // Stops the form from automaticly submitting
    if (formValid(this.state)) {
      const { applicant, areacode, comment, email, name, phone } = this.state;
      let bodyForm = new FormData();
      bodyForm.append('applicant', applicant);
      bodyForm.append('areacode', areacode);
      bodyForm.append('comment', comment);
      bodyForm.append('email', email);
      bodyForm.append('name', name);
      bodyForm.append('phone', phone);
      axios({
        method: 'POST',
        url: 'https://heksemel.no/case/submit.php',
        data: bodyForm,
        config: {headers: {'Content-Type': 'multipart/form-data'}}
      })
        .catch(res => {
          console.log(res)
        })
    }
    else {
      console.log('Klarte ikke å sende inn skjemaet');
    }*/
  };

  handleChange = e => {
    e.preventDefault();
    let { name, value } = e.target;
    let formErrors = this.state.formErrors;
    /*
    * Sjekker etter feil i inputen det skrives i. Utifra hvilket name vi har i target, sjekker vi om value følger konvensjonene for valgt type.
    * Mobilnummer tar hensyn til om nummeret skrives med mellomrom, og om det skrives med landskode.
    * */
    switch (name) {
      case 'name':
        if (value.length === 0) formErrors.name = 'Navn kan ikke være tom. ';
        if (!/^[a-zæøåA-ZÆØÅ ]+$/.test(value)) formErrors.name = 'Navn kan kun inneholde bokstaver ';
        else formErrors.name = '';
        break;
      case 'email':
        if (value.length === 0) formErrors.email = 'Email kan ikke være tom';
        if (!/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value)) formErrors.email = 'Skriv inn en godkjent mail';
        else formErrors.email = '';
        break;
      case 'phone':
        value = value.replace(/\s+/g,'');
        if (value.length === 0 || value < 0) formErrors.phone = 'Mobilnummer kan ikke være tom';
        if (value[0] === '+') {
          value = value.slice(1, value.length);
          console.log(value)
          if (value.length !== 10 || !/^[0-9]+$/.test(value)) formErrors.phone = 'Mobilnummeret ser ikke ut til å være riktig';
          else formErrors.phone = '';
        }
        else if (value[0] === '0' && value[1] === '0') {
          if (value.length !== 12 || !/^[0-9]+$/.test(value)) formErrors.phone = 'Mobilnummeret ser ikke ut til å være riktig';
          else formErrors.phone = '';
        }
        else if (value.length !== 8 || !/^[0-9]+$/.test(value))  formErrors.phone = 'Mobilnummeret ser ikke ut til å være riktig';
        else formErrors.phone = '';
        break;
      case 'areacode':
        if (value.length !== 4 || value < 0 || !/^[0-9]+$/.test(value)) formErrors.areacode = 'Postnummer må være 4 sifre.';
        else formErrors.areacode = '';
        break;
      default:
        formErrors.name = '';
        formErrors.email = '';
        formErrors.phone = '';
        formErrors.areacode = 0;
        break;
    }
    /*
    * Her oppdaterer vi feilene våre, og styrer også om submit knappen er enabled eller disabled
    * */
    this.setState({formErrors, [name]: value });
    if (formValid(this.state)) this.setState({disabled: false});
    else this.setState({disabled: true})
  };

  render () {
    const { formErrors } = this.state;

    return (
      <div className="flex-container">
        <div className="card">
          <div className="text">
            <form onSubmit={this.handleSubmit} action="https://heksemel.no/case/submit.php" method="post" noValidate>
              <legend><h3>Informasjon</h3></legend>
              <div className="name">
                <input
                  type="text"
                  className={formErrors.name.length > 0 ? 'error' : null}
                  name="name"
                  placeholder="Navn"
                  onChange={this.handleChange}
                />
              </div>
              <div className="errorDiv">
                {formErrors.name.length > 0 && (
                  <span className="errorMessage">{formErrors.name} </span>
                )}
              </div>
              <div className="email">
                <input
                  type="email"
                  className={formErrors.email.length > 0 ? 'error' : null}
                  name="email"
                  placeholder="E-post"
                  onChange={this.handleChange}
                />
              </div>
              <div className="errorDiv">
                {formErrors.email.length > 0 && (
                  <span className="errorMessage">{formErrors.email} </span>
                )}
              </div>
              <div className="phone">
                <input
                  type="tel"
                  className={formErrors.phone.length > 0 ? 'error' : null}
                  name="phone"
                  placeholder="Telefonnummer"
                  onChange={this.handleChange}
                />
              </div>
              <div className="errorDiv">
                {formErrors.phone.length > 0 && (
                  <span className="errorMessage">{formErrors.phone} </span>
                )}
              </div>
              <div className="areacode">
                <input
                  type="number"
                  className={formErrors.areacode.length > 0 ? 'error' : null}
                  name="areacode"
                  placeholder="Poststed"
                  min="0"
                  max="9999"
                  onChange={this.handleChange}
                />
              </div>
              <div className="errorDiv">
                {formErrors.areacode.length > 0 && (
                  <span className="errorMessage">{formErrors.areacode} </span>
                )}
              </div>
              <div className="comment">
                <textarea name="comment" placeholder="Kommentar" onChange={this.handleChange}/>
              </div>
              <input type="hidden" name="applicant" value="Jesper"/>
              <div className={this.state.disabled ? 'disabled' : 'button'}>
                <button type="submit" name="submit" disabled={this.state.disabled}>SEND INN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
