# Case for Nettbureau

## Oppbygging
Dette prosjektet inneholder to mapper: standard og react_form. Standard er html-formet, mens react_form er et react prosjekt.

## Komme i gang i react-prosjektet
1. Naviger til react_form i terminalen. $ cd react_forem
2. Installer avhengigheter med yarn install
3. Kjør prosjektet med $ yarn start

## Kommentarer
Dette var mitt første React-prosjekt. Alt i alt fornøyd, rimelig stor overgang fra Vue, men likte absolutt React tankegangen.
Fikk No-Cors da jeg prøvde å poste formet med Axios, så endte opp med å submitte på den gamle måten, og disablesubmit knappen til
formet var godkjent. Er klar over at det er ganske enkelt å endre denne til Enabled via Inspector viduet på nettlesere, men ettersom
det ikke er krise om skjemaet sendes inn tomt, lot jeg det bli med denne løsningen. Likevel hadde en bedre løsning vært å bruke `e.preventDefault()` i `onSumbit()`
og axios og på denne måten bruke javaScript til å avgjøre om skjemaet skulle bli sendt. 
